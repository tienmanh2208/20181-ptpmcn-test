var newEmployee = {};
var count = 5;

var tmp = '<div class="flex-row" id="new">'
        + '<div class="stt mgr-10 border-top" id="newstt"></div>'
        + '<div class="col mgr-10 border-top" id="newten"></div>'
        + '<div class="col mgr-10 border-top ta-center" id="newsdt"></div>'
        + '<div class="col mgr-10 border-top ta-center" id="newpb"></div>'
        + '<div class="col mgr-10 border-top" id="newtt"></div>'
        + '<div id="ft" class="emp-feature ta-center" hidden><i id="edit" class="far fa-edit"></i> <i id="delete" class="fas fa-user-minus"></i></div>'
        + '</div>';

$(document).ready(function(){
    for(var i = 1; i <= count; ++i){
        setMouseEventForEmp(i);
    }

    $('#adtnv').click(function(){
        loadModal('newEmployee','show', function(){
            $('#saveInfo').unbind('click').click(function(){
                getInfoEmp();
                $('#newEmployee').modal('hide');
                count++;
                empInfo(count, newEmployee);
                setMouseEventForEmp(count);
            });
        })
    })
});

/**
 * 
 * @param {*} id not contain #
 * @param {*} action show or hide
 * @param {*} callback function will be called after all the actions are executed
 */
function loadModal(id, action, callback){
    if(callback) $('#' + id).on('shown.bs.modal', callback).modal(action);
    else $('#' + id).modal(action);
}

/**
 * Set info for an employee
 * @param {number} index index of employee
 * @param {object} info object contain info of this employee
 * @param {*} callback 
 */
function empInfo(index, info, callback){
    $('#list-employee').append(tmp);
    $('#new').attr('id', index);
    $('#newstt').text(index).attr('id', index + 'stt');
    $('#newten').text(info.name).attr('id', index + 'ten');
    $('#newsdt').text(info.phonenumber).attr('id', index + 'sdt');
    $('#newpb').text(info.position).attr('id', index + 'pb');
    $('#ft').attr('id', index + 'ft');
    $('#edit').attr('id', index + 'edit');
    $('#delete').attr('id', index + 'delete');
    if(info.state === 'Active') $('#newtt').append('<i style="color: green;" class="fas fa-circle"></i>').append(' ' + info.state).attr('id', index + 'tt');
    else if(info.state === 'Deactive') $('#newtt').append('<i style="color: gray;" class="fas fa-circle"></i>').append(' ' + info.state).attr('id', index + 'tt');    

    if(callback) callback;
}

/**
 * Make icon appear and disappear
 * @param {number} id
 * @param {*} callback 
 */
function setMouseEventForEmp(id, callback){
    $('#' + id + 'edit').click(function(){
        loadModal('newEmployee','show', function(){
            $('#saveInfo').unbind('click').click(function(){
                alert('This icon is not setted any action, dont try to click this button again!!!');
                // getInfoEmp();
                // $('#newEmployee').modal('hide');
                // count++;
                // empInfo(count, newEmployee);
            });
        })
    });

    $('#' + id + 'delete').click(function(){
        alert('This icon is not setted any action, dont try to click this button again!!!');
    });

    $('#' + id).mouseenter(function(){
        $('#' + id + 'ft').removeAttr('hidden');
    }).mouseleave(function(){
        $('#' + id + 'ft').attr('hidden','hidden');
    });

    if(callback) callback;
}

function getInfoEmp(){
    newEmployee.name = $('#md-name').val();
    newEmployee.dateOfBirth = $('#md-dateofbirth').val();
    newEmployee.identifycard = $('#md-identifycard').val();
    newEmployee.mail = $('#md-mail').val();
    newEmployee.gender = $('#md-gender').val();
    newEmployee.phonenumber = $('#md-phonenumber').val();
    newEmployee.position = $('#md-position').val();
    newEmployee.address = $('#md-address').val();
    newEmployee.photo = $('#md-photo').val();
    newEmployee.state = $('#md-state').val();
}