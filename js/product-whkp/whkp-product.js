var addProduct = {};
count = 2;
var current = 0;

$(document).ready(function(){
    for(var i = 1; i <= count; ++i){
        setMouseEventForWhPro(i);
    }

    // $('#adtsp').click(function(){
    //     loadModal('newProduct','show', function(){
    //         $('#saveInfo').unbind('click').click(function(){
    //             getInfoWhPro();
    //             $('#newProduct').modal('hide');
    //             count++;
    //             proInfo(count, addProduct);
    //             setMouseEventForWhPro(count);
    //         });
    //     })
    // })
});

/**
 * 
 * @param {*} id not contain #
 * @param {*} action show or hide
 * @param {*} callback function will be called after all the actions are executed
 */
function loadModal(id, action, callback){
    if(callback) $('#' + id).on('shown.bs.modal', callback).modal(action);
    else $('#' + id).modal(action);
}

/**
 * Set info for an employee
 * @param {number} index index of employee
 * @param {object} info object contain info of this employee
 * @param {*} callback 
 */
function whproInfo(index, info, callback){
    var tmp = $('#' + index + 'sl').text();
    $('#' + index + 'sl').text(parseInt(info.quantity) + parseInt(tmp));

    if(callback) callback;
}

/**
 * Make icon appear and disappear
 * @param {number} id
 * @param {*} callback 
 */
function setMouseEventForWhPro(id, callback){
    $('#' + id + 'add').click(function(){
        loadModal('addProduct','show', function(){
            $('#saveInfo').unbind('click').click(function(){
                getInfoWhPro();
                clearModal();
                whproInfo(current, addProduct);

                loadModal('addProduct', 'hide');
                // getInfoEmp();
                // $('#addProduct').modal('hide');
                // count++;
                // empInfo(count, addProduct);
            });
        })

        current = parseInt($(this).parent().parent().attr('id'));
    });

    if(callback) callback;
}

function getInfoWhPro(){
    addProduct.name = $('#md-name').val();
    addProduct.hsx = $('#md-hsx').val();
    addProduct.date = $('#md-date').val();
    addProduct.quantity = $('#md-quantity').val();
}

function clearModal(){
    $('#md-name').val('');
    $('#md-hsx').val('');
    $('#md-date').val('');
    $('#md-quantity').val('');
}