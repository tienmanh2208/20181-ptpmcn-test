function drawABarChart(id, data){
    var chart = new CanvasJS.Chart(id, {
        animationEnabled: true,
        theme: "light2",
        // title: {
        //     text: "Số lượng hàng đã bán theo ngày"
        // },
        axisY: {
            title: "Số lượng",
            titleFontSize: 24
        },
        data: [{
            type: "column",
            yValueFormatString: "#,### mặt hàng",
            dataPoints: data
        }]
    });

    chart.render();
}

function drawALineChart(id,data){
    var chart = new CanvasJS.Chart(id, {
        animationEnabled: true,
        // title:{
        //     text: "Tổng số tiền thu được theo ngày"
        // },
        axisX:{
            valueFormatString: "DD MMM",
            crosshair: {
                enabled: true,
                snapToDataPoint: true
            }
        },
        axisY: {
            title: "Số tiền",
            includeZero: false,
            valueFormatString: "VNĐ##0.00",
            crosshair: {
                enabled: true,
                snapToDataPoint: true,
                labelFormatter: function(e) {
                    return CanvasJS.formatNumber(e.value, "##0.00") + 'VNĐ';
                }
            }
        },
        data: [{
            type: "area",
            xValueFormatString: "DD MMM",
            yValueFormatString: "$##0.00",
            dataPoints: data
        }]
    });
    chart.render();
}

/**
 * No date
 * @param {*} id 
 * @param {*} data 
 */
function drawAColumnChart(id, data){
    
    var chart = new CanvasJS.Chart(id, {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        // title:{
        //     text: "Số đơn đã đặt"
        // },
        axisY: {
            title: "Số lượng đơn"
        },
        data: [{        
            type: "column",  
            showInLegend: true, 
            legendMarkerColor: "grey",
            legendText: "Đơn vị : Số đơn / ngày",
            dataPoints: data,
        }]
    });
    chart.render();
}

function drawASplineChart(id, data){
    var chart = new CanvasJS.Chart(id, {
        animationEnabled: true,  
        // title:{
        //     text: "Số đơn đã chuyển theo ngày"
        // },
        axisY: {
            title: "Số đơn đã chuyển",
            valueFormatString: "#0,,.",
            suffix: "mn",
            // stripLines: [{
            //     value: 3366500,
            //     label: "Average"
            // }]
        },
        data: [{
            yValueFormatString: "#,### Đơn",
            xValueFormatString: "YYYY",
            type: "spline",
            dataPoints: data,
        }]
    });
    chart.render();
}