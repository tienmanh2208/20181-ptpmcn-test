var chartSoLuongHangBan = null;
var dataSoLuongHangBan = [
    { x: new Date(2016, 7, 22), y: 80 },
    { x: new Date(2016, 7, 24), y: 92 },
    { x: new Date(2016, 7, 25), y: 60 },
    { x: new Date(2016, 7, 26), y: 73 },
    { x: new Date(2016, 7, 29), y: 89 },
    { x: new Date(2016, 7, 30), y: 120 },
    { x: new Date(2016, 7, 31), y: 30 }
];
var chartSoTien = null;
var dataSoTien = [
    { x: new Date(2016, 7, 18), y: 78.899002 },
    { x: new Date(2016, 7, 19), y: 77.127998 },
    { x: new Date(2016, 7, 22), y: 76.759003 },
    { x: new Date(2016, 7, 24), y: 77.623001 },
    { x: new Date(2016, 7, 25), y: 76.408997 },
    { x: new Date(2016, 7, 26), y: 76.041000 },
    { x: new Date(2016, 7, 29), y: 76.778999 },
    { x: new Date(2016, 7, 30), y: 78.654999 },
    { x: new Date(2016, 7, 31), y: 77.667000 }
];
var chartSoDonDatHang = null;
var dataSoDonDatHang = [
    { y: 300878, label: "Venezuela" },
    { y: 266455,  label: "Saudi" },
    { y: 169709,  label: "Canada" },
    { y: 158400,  label: "Iran" },
    { y: 142503,  label: "Iraq" },
    { y: 101500, label: "Kuwait" },
    { y: 97800,  label: "UAE" },
    { y: 80000,  label: "Russia" }
];
var chartSoDonDaShip = null;
var dataSoDonDaShip = [
    {x: new Date(2002, 0), y: 2506000},
    {x: new Date(2003, 0), y: 2798000},
    {x: new Date(2004, 0), y: 3386000},
    {x: new Date(2005, 0), y: 6944000},
    {x: new Date(2006, 0), y: 6026000},
    {x: new Date(2007, 0), y: 2394000},
    {x: new Date(2008, 0), y: 1872000},
    {x: new Date(2009, 0), y: 2140000},
];

$(document).ready(function(){
  drawABarChart('r1c1chart',dataSoLuongHangBan);
  drawALineChart('r1c2chart',dataSoTien);
  drawAColumnChart('r2c1chart', dataSoDonDatHang);
  drawASplineChart('r2c2chart', dataSoDonDaShip);
})