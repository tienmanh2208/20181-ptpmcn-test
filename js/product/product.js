var newEmployee = {};
var count = 2;

var tmp = '<div class="flex-row" id="new">'
        + '<div class="stt mgr-10 border-top" id="newstt"></div>'
        + '<div class="col mgr-10 border-top" id="newten"></div>'
        + '<div class="col mgr-10 border-top ta-center" id="newsl"></div>'
        + '<div class="col mgr-10 border-top ta-center" id="newidsp"></div>'
        + '<div class="col mgr-10 border-top ta-center" id="newcn"><i id="newedit" class="fas fa-pen-square proedit"></i><i id="newdelete" class="fas fa-trash prodelete"></i></div>'
        + '</div>';

$(document).ready(function(){
    for(var i = 1; i <= count; ++i){
        setMouseEventForPro(i);
    }

    $('#adtsp').click(function(){
        loadModal('newProduct','show', function(){
            $('#saveInfo').unbind('click').click(function(){
                getInfoPro();
                $('#newProduct').modal('hide');
                count++;
                proInfo(count, newEmployee);
                setMouseEventForPro(count);
            });
        })
    })
});

/**
 * 
 * @param {*} id not contain #
 * @param {*} action show or hide
 * @param {*} callback function will be called after all the actions are executed
 */
function loadModal(id, action, callback){
    if(callback) $('#' + id).on('shown.bs.modal', callback).modal(action);
    else $('#' + id).modal(action);
}

/**
 * Set info for an employee
 * @param {number} index index of employee
 * @param {object} info object contain info of this employee
 * @param {*} callback 
 */
function proInfo(index, info, callback){
    $('#list-product').append(tmp);
    $('#new').attr('id', index);
    $('#newstt').text(index).attr('id', index + 'stt');
    $('#newten').text(info.name).attr('id', index + 'ten');
    $('#newsl').text('0').attr('id', index + 'sl');
    $('#newidsp').text(index + 'abc').attr('id', index + 'idsp');
    $('#newedit').attr('id', index + 'edit');
    $('#newdelete').attr('id', index + 'delete');

    if(callback) callback;
}

/**
 * Make icon appear and disappear
 * @param {number} id
 * @param {*} callback 
 */
function setMouseEventForPro(id, callback){
    $('#' + id + 'edit').click(function(){
        loadModal('newProduct','show', function(){
            $('#saveInfo').unbind('click').click(function(){
                alert('This icon is not setted any action, dont try to click this button again!!!');
                // getInfoEmp();
                // $('#newEmployee').modal('hide');
                // count++;
                // empInfo(count, newEmployee);
            });
        })
    });

    $('#' + id + 'delete').click(function(){
        alert('This icon is not setted any action, dont try to click this button again!!!');
    });

    if(callback) callback;
}

function getInfoPro(){
    newEmployee.name = $('#md-name').val();
    newEmployee.hsx = $('#md-hsx').val();
    newEmployee.date = $('#md-date').val();
    newEmployee.extraProperties = $('#md-extraProperties').val();
}